import {combineReducers} from 'redux';
import {MapViewReducer} from "./modules/map-view/reducer";


export default combineReducers({
    mapViewModel: MapViewReducer
});
