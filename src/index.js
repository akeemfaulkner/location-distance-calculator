import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux'
import {compose, createStore} from 'redux';

import App from './App';
import registerServiceWorker from './registerServiceWorker';
import baseReducer from './reducers';
import './styles/app.css';
import applyMiddleware from "redux/es/applyMiddleware";
import {MapViewMiddleware} from "./modules/map-view/map-view-middleware";

const enhancer = compose(
    applyMiddleware(MapViewMiddleware),
    window.devToolsExtension ? window.devToolsExtension() : f => f
);

/* eslint-disable no-underscore-dangle */
const store = createStore(
    baseReducer,
    enhancer,
);
/* eslint-enable */

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('root'));
registerServiceWorker();
