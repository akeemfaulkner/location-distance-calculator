/*global google*/
import * as Rx from 'rxjs/bundles/Rx.min';


export class MapApiServiceClass {

    constructor(onLoaded, parentContainer = document.body, apiKey = '') {
        this.url = 'https://maps.googleapis.com/maps/api/js?key=';
        this.map = {};
        this.markers = {};
        this.onLoaded = onLoaded;
        this.apiKey = apiKey;
        this.createScriptTag_(parentContainer);
    }

    setDefaultCoordinates(position) {
        this.defaultLatitude_ = position.coords.latitude;
        this.defaultLongitude_ = position.coords.longitude;
    }

    init() {
        this.initGoogleMapsDependencies_();
    }

    /**
     *
     * @param {Element} container
     * @private
     */
    createScriptTag_(container) {
        const googleMapScript = document.createElement('script');
        googleMapScript.setAttribute('src', `${this.url}${this.apiKey}`);
        container.appendChild(googleMapScript);
    }

    /**
     * Check is google is in the window object
     *
     * @returns {boolean}
     * @private
     */
    hasGoogleMapsLoaded_() {
        return ('google' in window);
    }

    /**
     * @private
     */
    initGoogleMapsDependencies_() {

        Rx.Observable.interval(1000)
            .map(() => {
                try {
                    this.geocoder = new google.maps.Geocoder();
                    return new Rx.Subject() && this.initialiseGoogleMapHandlers_();
                } catch (e) {
                    return Rx.Observable.throw(new Error(e));
                }
            })
            .retry(3)
            .take(1)
            .subscribe();

    }

    initialiseGoogleMapHandlers_() {
        if (!this.hasGoogleMapsLoaded_()) return;
        this.onLoaded && this.onLoaded();
    }

    fetchCurrentPosition(callback) {
        navigator.geolocation && navigator.geolocation.getCurrentPosition(({coords: {latitude, longitude}}) => {
            this.currentPosition = {lat: latitude, lng: longitude};
            callback && callback(this.currentPosition);
        });
    }

    addMap(key, mapContainer) {

        if (!this.hasGoogleMapsLoaded_()) return;

        const add = (position) => {
            this.map[key] = new google.maps.Map(mapContainer, {
                center: position,
                zoom: 12
            });

            setTimeout(
                () => google.maps.event.trigger(this.map[key], 'resize'),
                500
            )
        };

        if (!this.currentPosition) {
            this.fetchCurrentPosition(add.bind(this));
        } else {
            add(this.currentPosition)
        }


    }

    /**
     * Find a list of addresses that match the search query param 'address'
     *
     * @param (String) address
     * @returns {Promise<T[]>|Promise}
     */
    findAddressesBy(address) {
        return new Promise((resolve, reject) => {
            this.geocoder.geocode({'address': address}, function (results, status) {
                if (status === 'OK') {
                    resolve(results);
                } else {
                    let errorMessage = 'Geocode was not successful for the following reason: ' + status;
                    console.error(errorMessage);
                    reject(errorMessage);
                }
            });
        });

    }

    setMapPoint(key, position) {
        if (!this.hasGoogleMapsLoaded_()) return;
        let map = this.map[key];
        if (!map) return;
        map.setCenter(position);
        this.markers[key] = this.markers[key] || [];
        this.clearMarkers(key);
        this.markers[key].push(
            new google.maps.Marker({
                map: map,
                position
            })
        );
    }

    clearMarkers(key) {
        let markers = this.markers[key];
        if (!markers) return;
        for (let i = 0; i < markers.length; i++) {
            markers[i].setMap(null);
        }
        this.markers[key] = [];
    }

}

export const MapApiService = new MapApiServiceClass();
