import {MapApiServiceClass} from "./map-api-service";


describe('map-api-service', () => {

    it('should accept the api key and generate a create a script tag on #init(apiKey)', () => {

        const container = document.createElement('div');

        const initialChildrenLength = container.childNodes.length;
        expect(initialChildrenLength).toBe(0);

        let apiKey = 123;
        export const mapApiService = new MapApiServiceClass(()=>{}, container, apiKey);

        const afterChildrenLength = container.childNodes.length;
        expect(afterChildrenLength).toBe(1);

        expect(container.firstChild.tagName).toEqual('SCRIPT');
        expect(container.firstChild.src).toBe('https://maps.googleapis.com/maps/api/js?key=' + apiKey);
    });
});