import {DistanceCalculationService} from './distance-calculation-service';

describe('distance-calculation-service', () => {

    const LOCATION_1 = {
        latitude:	51.5839911224,
        longitude:	-0.101655688712
    };

    const LOCATION_2 = {
        latitude:	51.5328509547,
        longitude:	-0.0878229504224
    };

    describe('distance-calculation-service members', () => {

        it('should contain the Unit.Kilometer', () => {
            expect(DistanceCalculationService.Unit.Kilometer).toBeDefined();
        });

        it('should contain the Unit.NauticalMiles', () => {
            expect(DistanceCalculationService.Unit.NauticalMiles).toBeDefined();
        });

        it('should contain the Unit.Miles', () => {
            expect(DistanceCalculationService.Unit.Miles).toBeDefined();
        });

    });

    describe('distance-calculation-service #calculate(lat1, lon1, lat2, lon2, opt_unit)', () => {
        let calculate = DistanceCalculationService.calculate.bind(DistanceCalculationService, LOCATION_1.latitude, LOCATION_1.longitude, LOCATION_2.latitude, LOCATION_2.longitude);

        it('should return Kilometers if opt_unit === DistanceCalculationService.Unit.Kilometer', () => {
            const expectedResult = '5.8';
            let calculateKillometer = calculate(DistanceCalculationService.Unit.Kilometer);
            expect(calculateKillometer.toFixed(1)).toEqual(expectedResult);
        });

        it('should return Nautical Miles if opt_unit === DistanceCalculationService.Unit.NauticalMiles', () => {
            const expectedResult = '3.1';
            let calculateNauticalMiles = calculate(DistanceCalculationService.Unit.NauticalMiles);
            expect(calculateNauticalMiles.toFixed(1)).toEqual(expectedResult);
        });

        it('should return Miles if opt_unit === DistanceCalculationService.Unit.Miles', () => {
            const expectedResult = '3.6';
            let calculateMiles = calculate(DistanceCalculationService.Unit.Miles);
            expect(calculateMiles.toFixed(1)).toEqual(expectedResult);
        });

        it('should return Miles if !opt_unit in  DistanceCalculationService.Unit', () => {
            const expectedResult = '3.6';
            let calculateMiles = calculate();
            expect(calculateMiles.toFixed(1)).toEqual(expectedResult);
        });

        
    });

});
