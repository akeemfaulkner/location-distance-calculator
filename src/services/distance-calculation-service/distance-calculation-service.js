export class DistanceCalculationServiceClass {

    /**
     * Available units for distance calulation
     * @enum {String}
     */
    Unit = {
        Kilometer: 'K',
        NauticalMiles: 'N',
        Miles: 'M',
    };

    /**
     * Converts statute miles to kilometers
     * @param miles
     */
    statuteMilesToKilometers(miles) {
        return miles * 1.609344;
    }


    /**
     * Converts statute miles to kilometers
     * @param miles
     */
    statuteMilesToKNauticalMiles(miles) {
        return miles * 0.8684;
    }

    /**
     *
     * Calculate the distance between two sets of latitude and longitude and return a distance in the given units
     * defined in @enum {String} Unit
     *
     * @param {Number} lat1
     * @param {Number} lon1
     * @param {Number} lat2
     * @param {Number} lon2
     * @param {DistanceCalculationServiceClass.Unit=} opt_unit
     */
    calculate(lat1, lon1, lat2, lon2, opt_unit) {

        const radlat1 = Math.PI * lat1 / 180;
        const radlat2 = Math.PI * lat2 / 180;
        const theta = lon1 - lon2;
        const radtheta = Math.PI * theta / 180;

        let conversion;
        let dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);

        dist = Math.acos(dist);
        dist = dist * 180 / Math.PI;
        dist = dist * 60 * 1.1515;

        if (opt_unit === "M" || !opt_unit)  return dist;
        if (opt_unit === "K") conversion = this.statuteMilesToKilometers;
        if (opt_unit === "N") conversion = this.statuteMilesToKNauticalMiles;

        return conversion && conversion(dist);



    }
}

export const DistanceCalculationService = new DistanceCalculationServiceClass();
