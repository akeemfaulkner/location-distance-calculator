export class BaseReducer {

    constructor(){
        this.reduce = this.reduce.bind(this);
    }

    getActionMap() {
        return this.actionMap_ || {};
    }

    getInitialState() {
        return {};
    }

    reduce(state, action) {
        let actionMap = this.getActionMap();
        let handler = actionMap[action.type];
        return handler ?  handler(state, action) : state || this.getInitialState();
    }

}
