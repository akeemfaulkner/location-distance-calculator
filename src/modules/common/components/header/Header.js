import React from 'react';

import {getClassNameFromNamespace} from "../../utils/css-utils";
import './Header.css';

const getClassName = getClassNameFromNamespace('common-components-header');

export const Header = () => (
    <div className={getClassName('container')}>
        <h1 className={getClassName('header')}>Location distance calculator</h1>
    </div>
);
