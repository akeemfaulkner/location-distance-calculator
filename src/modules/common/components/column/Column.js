import React, {Component} from 'react'
import PropTypes from 'prop-types';

export class Column extends Component {


    getStyles() {
        let {width} = this.props;
        return {
            flexBasis: `${width}%`
        };
    }

    render() {
        let {children, className = ''} = this.props;
        return (
            <div style={this.getStyles()} className={className}>
                {children}
            </div>
        );
    }

}

Column.propTypes = {
    children: PropTypes.any.isRequired,
    width: PropTypes.number,
};
