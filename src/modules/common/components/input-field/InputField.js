import React, {Component} from 'react';


import {getClassNameFromNamespace} from "../../utils/css-utils";
import './InputField.css';

const getClassName = getClassNameFromNamespace('common-components-input-field');


export class InputField extends Component {

    render() {
        let {
            label,
            type,
            value,
            onChange,
            onFocus,
            placeholder = ''
        } = this.props;

        return (
            <label className={getClassName()}>
                {label}
                <input
                    type={type}
                    defaultValue={value}
                    onChange={onChange}
                    onFocus={onFocus}
                    placeholder={placeholder}
                />
            </label>
        );
    }
}
