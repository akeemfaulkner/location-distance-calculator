import {Component} from 'react'
import PropTypes from 'prop-types';

export class If extends Component {

    render() {
        let {condition, children} = this.props;
        return condition ? children : null;
    }

}

If.PropTypes = {
    children: PropTypes.element.isRequired,
    condition: PropTypes.bool.isRequired
};

