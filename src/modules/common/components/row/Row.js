import React, {Component} from 'react'

import {getClassNameFromNamespace} from "../../utils/css-utils";
import './Row.css';

const getClassName = getClassNameFromNamespace('common-components-row');

export class Row extends Component {

    render() {
        let {children} = this.props;
        return (
            <div className={getClassName('container')}>
                {children}
            </div>
        );
    }

}