export const getClassNameFromNamespace = namespace => className => namespace + (className ? '__' + className : '');
