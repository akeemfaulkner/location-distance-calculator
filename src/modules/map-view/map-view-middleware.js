import {MapViewActionType} from "./actions/actions";
import {MapApiService} from "../../services/map-api-service/map-api-service";
import {LocationResponseModel} from "./model/location-response";
import {setDistance, updateSearchQueryFromSuccess, updateSearchQueryToSuccess} from "./actions/actionCreators";
import {DistanceCalculationService} from "../../services/distance-calculation-service/distance-calculation-service";

function handleCalculateDistance(action, dispatch, {mapViewModel}) {

    let {
        fromLocation,
        toLocation
    } = mapViewModel;

    if (action.type !== MapViewActionType.CALCULATE_DISTANCE) return;
    if (typeof fromLocation.longitude === 'undefined' || typeof toLocation === 'undefined')  return;

    let distance = DistanceCalculationService.calculate(
        fromLocation.latitude,
        fromLocation.longitude,
        toLocation.latitude,
        toLocation.longitude,
        action.payload
    );

    dispatch(setDistance(distance));
}
function handleUpdateFromSearchQuery(action, dispatch) {
    if (action.type !== MapViewActionType.UPDATE_FROM_SEARCH_QUERY) return;
    handleUpdateSearchQuery(action, (locations) => {
        dispatch(updateSearchQueryFromSuccess(locations));
    });
}

function handleUpdateToSearchQuery(action, dispatch) {
    if (action.type !== MapViewActionType.UPDATE_TO_SEARCH_QUERY) return;
    handleUpdateSearchQuery(action, (locations) => {
        dispatch(updateSearchQueryToSuccess(locations));
    });
}


export const MapViewMiddleware = store => next => action => {
    let state = store.getState();
    let dispatch = store.dispatch;
    handleUpdateFromSearchQuery(action, dispatch, state);
    handleUpdateToSearchQuery(action, dispatch, state);
    handleCalculateDistance(action, dispatch, state);
    next(action)
};


let throttleTimeout;
function handleUpdateSearchQuery(action, onResolve) {
    clearTimeout(throttleTimeout);
    throttleTimeout = setTimeout(() =>
            MapApiService.findAddressesBy(action.payload).then(response => {
                let locations = (response || []).map(location => new LocationResponseModel(location));
                onResolve(locations)
            }),
        300);
}
