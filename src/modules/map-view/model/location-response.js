export class LocationResponseModel {

    constructor(response) {
        this.name = response.formatted_address;
        this.location = {
            latitude: response.geometry.location.lat(),
            longitude: response.geometry.location.lng(),
        }
    }

}
