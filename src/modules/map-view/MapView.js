import React, {Component} from 'react';

import {Row} from "../common/components/row/Row";
import {Map} from "./components/map/Map";
import {AddressLocator} from "./components/address-locator/AddressLocator";
import {MapApiService} from "../../services/map-api-service/map-api-service";
import ReactDOM from "react-dom";
import {connect} from "react-redux";

export class MapViewContainer extends Component {

    mapNames = {
        mapFrom: 'mapFrom',
        mapTo: 'mapTo',
    };

    componentWillMount() {
        this.MapApiService = MapApiService;
        this.MapApiService.onLoaded = () => {
            let mapFrom = this.getDomNode(this.mapFrom);
            let mapTo = this.getDomNode(this.mapTo);

            this.MapApiService.addMap(this.mapNames.mapFrom, mapFrom);
            this.MapApiService.addMap(this.mapNames.mapTo, mapTo);
        };
    }

    componentWillUpdate(nextProps) {

        let {mapViewModel} = this.props;

        let nextFromLocation = JSON.stringify(nextProps.mapViewModel.fromLocation);
        let nextToLocation = JSON.stringify(nextProps.mapViewModel.toLocation);

        let fromLocation = JSON.stringify(mapViewModel.fromLocation);
        let toLocation = JSON.stringify(mapViewModel.toLocation);

        if (nextFromLocation !== fromLocation) {
            let fromLocationObj = nextProps.mapViewModel.fromLocation;
            this.MapApiService.setMapPoint(this.mapNames.mapFrom, {lat: fromLocationObj.latitude, lng: fromLocationObj.longitude});
        }

        if (nextToLocation !== toLocation) {
            let toLocationObj = nextProps.mapViewModel.toLocation;
            this.MapApiService.setMapPoint(this.mapNames.mapTo, {lat: toLocationObj.latitude, lng: toLocationObj.longitude});
        }
    }

    componentDidMount() {
        this.MapApiService.init();
    }

    getDomNode(component) {
        return ReactDOM.findDOMNode(component);
    }

    render() {
        return (
            <div>
                <AddressLocator/>
                <Row>
                    <Map ref={e => this.mapFrom = e}/>
                    <Map ref={e => this.mapTo = e}/>
                </Row>
            </div>
        );
    }
}


function mapStateToProps({mapViewModel}) {
    return {mapViewModel};
}

export const MapView = connect(
    mapStateToProps
)(MapViewContainer);
