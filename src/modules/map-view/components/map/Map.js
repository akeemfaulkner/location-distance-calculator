import React, {Component} from 'react';
import {Column} from "../../../common/components/column/Column";

import './Map.css';
import {getClassNameFromNamespace} from "../../../common/utils/css-utils";

const getClassName = getClassNameFromNamespace('map-view-components-map');


export class Map extends Component {

    render() {
        return (
            <Column width={35} className={getClassName()}>loading...</Column>
        );
    }

}
