import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ReactDom from 'react-dom';

import {Column} from "../../../common/components/column/Column";
import {InputField} from "../../../common/components/input-field/InputField";
import {List} from "./List";
import {getClassNameFromNamespace} from "../../../common/utils/css-utils";
import './Control.css';
import {If} from "../../../common/components/condition/If";

const getClassName = getClassNameFromNamespace('map-view-components-address-locator-control');

export class Control extends Component {

    constructor(props) {
        super(props);

        this.inputFieldRef_ = null;

        this.state = {
            showList: false
        };

        this.handleListItemClick = this.handleListItemClick.bind(this);
        this.setShowList = this.setShowList.bind(this);
    }

    handleListItemClick(name, location) {
        let {onSetLocation} = this.props;
        let inputComponent = ReactDom.findDOMNode(this.inputFieldRef_);
        let inputElement = inputComponent.querySelector('input');
        inputElement.value = name;
        this.setState({showList: false});
        onSetLocation(location);
    }

    setShowList(showList = false){
        this.setState({showList});
    }

    render() {

        let {label, onChange, listData} = this.props;
        let {inputFieldValue, showList} = this.state;

        return (
            <Column width={45}>
                <div className={getClassName()}>
                    <InputField
                        ref={input => this.inputFieldRef_ = input}
                        label={label}
                        type={'text'}
                        value={inputFieldValue}
                        onFocus={()=>this.setShowList(true)}
                        onChange={(e) => onChange(e.target.value)}
                    />
                    <If condition={listData.length && showList}>
                        <List data={listData} onClick={this.handleListItemClick}/>
                    </If>
                </div>
            </Column>
        );
    }
}


Control.PropTypes = {
    value: PropTypes.string.isRequired,
    listData: PropTypes.arrayOf(PropTypes.any),
};
