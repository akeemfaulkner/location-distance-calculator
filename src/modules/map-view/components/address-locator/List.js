import React from 'react';

import {getClassNameFromNamespace} from "../../../common/utils/css-utils";
import './List.css';

const getClassName = getClassNameFromNamespace('map-view-components-address-locator-list');

export const ListItem = ({name, location, onClick}) => (
    <li className={getClassName('item')}>
        <button className={getClassName('link')} onClick={() => onClick(name, location)}>{name}</button>
    </li>
);

export const List = ({data, onClick}) => (
    <ul className={getClassName()}>
        {data.map((location, index) => <ListItem key={index} {...location} onClick={onClick}/>)}
    </ul>
);
