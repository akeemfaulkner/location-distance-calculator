import React, {Component} from 'react';
import PropTypes from 'prop-types';

import {Column} from "../../../common/components/column/Column";
import {getClassNameFromNamespace} from "../../../common/utils/css-utils";
import './Calculator.css';
import {DistanceCalculationService} from "../../../../services/distance-calculation-service/distance-calculation-service";

const getClassName = getClassNameFromNamespace('map-view-components-address-calculator');

const unit = DistanceCalculationService.Unit;

let textUnits = {
    [unit.Miles]: 'Miles',
    [unit.Kilometer]: 'Kilometers',
    [unit.NauticalMiles]: 'Nautical Miles',
};

export class Calculator extends Component {

    constructor(props) {
        super(props);
        this.state = {
            unit: unit.Miles
        };
        this.onChange = this.onChange.bind(this);
    }

    onChange({target: {value}}) {
        let {onClick} = this.props;
        this.setState({unit: value});
        onClick(value);
    }

    isDistanceDefined() {
        let {distance} = this.props;
        return typeof distance !== 'undefined' && distance !== null;
    }

    getTextUnit() {
        return this.isDistanceDefined() ? ' ' + textUnits[this.state.unit] : '';
    }

    getDistance() {
        let {distance} = this.props;
        return distance ? distance.toFixed(2) : '...';
    }

    render() {
        let {onClick} = this.props;

        return (
            <Column width={100}>
                <div className={getClassName()}>
                    <button onClick={() => onClick(this.state.unit)}>Calculate</button>
                    <select onChange={this.onChange}>
                        <option value={unit.Miles}>{textUnits[unit.Miles]}</option>
                        <option value={unit.Kilometer}>{textUnits[unit.Kilometer]}</option>
                        <option value={unit.NauticalMiles}>{textUnits[unit.NauticalMiles]}</option>
                    </select>
                    The distance is {this.getDistance()} {this.getTextUnit()}
                </div>
            </Column>
        );
    }
}

Calculator.PropTypes = {
    distance: PropTypes.number.isRequired,
    onClick: PropTypes.func.isRequired,
};

