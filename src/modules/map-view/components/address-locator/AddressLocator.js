import React, {Component} from 'react';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";

import {Row} from "../../../common/components/row/Row";
import {Control} from "./Control";
import * as mapViewActions from '../../actions/actionCreators'
import {Calculator} from "./Calculator";

export class AddressLocatorComponent extends Component {

    render() {

        let {
            actions: {
                mapView: {
                    updateFromSearchQuery,
                    updateToSearchQuery,
                    setFromLocation,
                    setToLocation,
                    calculateDistance
                }
            },
            mapViewModel: {
                fromResults,
                toResults,
                distance
            }
        } = this.props;

        return (
            <div>
                <Row>
                    <Calculator
                        onClick={calculateDistance}
                        distance={distance}
                    />
                </Row>
                <Row>
                    <Control
                        label={'Enter address from'}
                        onChange={updateFromSearchQuery}
                        listData={fromResults}
                        onSetLocation={setFromLocation}
                    />
                    <Control
                        label={'Enter address to'}
                        onChange={updateToSearchQuery}
                        listData={toResults}
                        onSetLocation={setToLocation}
                    />
                </Row>
            </div>
        );
    }
}

function mapStateToProps({mapViewModel}) {
    return {mapViewModel};
}

function mapDispatchToProps(dispatch) {
    return {
        actions: {
            mapView: bindActionCreators(mapViewActions, dispatch)
        }
    };
}

export const AddressLocator = connect(
    mapStateToProps,
    mapDispatchToProps
)(AddressLocatorComponent);
