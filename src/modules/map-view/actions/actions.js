export const MapViewActionType = {
    SET_USER_LOCATION: '[MapViewActionType] SET_USER_LOCATION',
    UPDATE_FROM_SEARCH_QUERY: '[MapViewActionType] UPDATE_FROM_SEARCH_QUERY',
    UPDATE_TO_SEARCH_QUERY: '[MapViewActionType] UPDATE_TO_SEARCH_QUERY',
    UPDATE_SEARCH_QUERY_FROM_SUCCESS: '[MapViewActionType] UPDATE_SEARCH_QUERY_FROM_SUCCESS',
    UPDATE_SEARCH_QUERY_TO_SUCCESS: '[MapViewActionType] UPDATE_SEARCH_QUERY_TO_SUCCESS',
    UPDATE_SEARCH_QUERY_ERROR: '[MapViewActionType] UPDATE_SEARCH_QUERY_ERROR',
    SET_FROM_LOCATION: '[MapViewActionType] SET_FROM_LOCATION',
    SET_TO_LOCATION: '[MapViewActionType] SET_TO_LOCATION',
    SET_DISTANCE: '[MapViewActionType] SET_DISTANCE',
    CALCULATE_DISTANCE: '[MapViewActionType] CALCULATE_DISTANCE',
};
