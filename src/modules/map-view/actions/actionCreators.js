import {MapViewActionType} from './actions';

export function setUserLocation(payload) {
    return {
        type: MapViewActionType.SET_USER_LOCATION,
        payload
    };
}

export function updateFromSearchQuery(payload) {
    return {
        type: MapViewActionType.UPDATE_FROM_SEARCH_QUERY,
        payload
    };
}
export function updateToSearchQuery(payload) {
    return {
        type: MapViewActionType.UPDATE_TO_SEARCH_QUERY,
        payload
    };
}

export function updateSearchQueryFromSuccess(payload) {
    return {
        type: MapViewActionType.UPDATE_SEARCH_QUERY_FROM_SUCCESS,
        payload
    };
}
export function updateSearchQueryToSuccess(payload) {
    return {
        type: MapViewActionType.UPDATE_SEARCH_QUERY_TO_SUCCESS,
        payload
    };
}

export function updateSearchQueryError(payload) {
    return {
        type: MapViewActionType.UPDATE_SEARCH_QUERY_ERROR,
        payload
    };
}


export function setFromLocation(payload) {
    return {
        type: MapViewActionType.SET_FROM_LOCATION,
        payload
    };
}
export function setToLocation(payload) {
    return {
        type: MapViewActionType.SET_TO_LOCATION,
        payload
    };
}
export function calculateDistance(payload) {
    return {
        type: MapViewActionType.CALCULATE_DISTANCE,
        payload
    };
}
export function setDistance(payload) {
    return {
        type: MapViewActionType.SET_DISTANCE,
        payload
    };
}
