import {BaseReducer} from "../common/base-reducer";
import {MapViewActionType} from "./actions/actions";

export class Reducer extends BaseReducer {

    constructor() {
        super();
        this.actionMap_ = {
            [MapViewActionType.SET_USER_LOCATION]: this.setUserLocation,
            [MapViewActionType.UPDATE_FROM_SEARCH_QUERY]: this.updateFromSearchQuery,
            [MapViewActionType.UPDATE_TO_SEARCH_QUERY]: this.updateToSearchQuery,
            [MapViewActionType.UPDATE_SEARCH_QUERY_FROM_SUCCESS]: this.updateSearchQueryFromSuccess,
            [MapViewActionType.UPDATE_SEARCH_QUERY_TO_SUCCESS]: this.updateSearchQueryToSuccess,
            [MapViewActionType.UPDATE_SEARCH_QUERY_ERROR]: this.updateSearchQueryError,
            [MapViewActionType.SET_FROM_LOCATION]: this.setFromLocation,
            [MapViewActionType.SET_TO_LOCATION]: this.setToLocation,
            [MapViewActionType.SET_DISTANCE]: this.setDistance,
        }
    }

    getInitialState(){
        return {
            userLocation : {},
            queryFrom : '',
            queryTo : '',
            fromResults : [],
            toResults : [],
            errors : {},
            fromLocation: {},
            toLocation: {},
            distance: null
        }
    }

    setUserLocation(state, action) {
        return {...state, ...{userLocation: action.payload}};
    }

    setFromLocation(state, action) {
        return {...state, ...{fromLocation: action.payload}};
    }

    setToLocation(state, action) {
        return {...state, ...{toLocation: action.payload}};
    }

    setDistance(state, action) {
        return {...state, ...{distance: action.payload}};
    }

    updateFromSearchQuery(state, action) {
         return {...state, ...{queryFrom: action.payload}};
    }

    updateToSearchQuery(state, action) {
         return {...state, ...{queryTo: action.payload}};
    }

    updateSearchQueryFromSuccess(state, action) {
         return {...state, ...{fromResults: action.payload}};
    }

    updateSearchQueryToSuccess(state, action) {
         return {...state, ...{toResults: action.payload}};
    }

    updateSearchQueryError(state, action) {
         return {...state, ...{errors: action.payload}};
    }

}

export const MapViewReducer = new Reducer().reduce;
