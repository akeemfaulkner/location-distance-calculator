import React, { Component } from 'react';

import {Header} from "./modules/common/components/header/Header";
import {MapView} from "./modules/map-view/MapView";

class App extends Component {

  render() {
    return (
      <div className="App">
          <Header/>
          <MapView/>
      </div>
    );
  }
}

export default App;
